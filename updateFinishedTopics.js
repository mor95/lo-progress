const _ = require('lodash')
const EventsEmitter = require('events')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.topics))
        throw new Error('Invalid topics')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    if(!(args.instance.events instanceof EventsEmitter))
        throw new Error('Invalid EventsEmitter instance')

    if(args.action === 'add'){
        args.method = 'addLOFinishedTopics'
    }
    else{
        if(args.action === 'delete'){
            args.method = 'deleteLOFinishedTopics'
        }
        else{
            throw new Error('Unknown action ' + args.action)
        }
    }

    return args.instance.APIInstance.makeRequest({
        component: 'finishedTopics',
        method: args.method,
        arguments: {
            shortName: args.instance.courseData.shortName,
            unit: args.instance.courseData.unit,
            topics: _.keys(args.topics)
        }
    })
    .then(function(response){
        if(response !== true){
            swal({
                type: 'error',
                title: 'Error en la respuesta del servidor.',
                text: 'Ocurrió un error al sincronizar tu progreso'
            })
            if(args.showFeedback !== false){
                console.error(response)
            }
            throw new Error('Expected a true response.')
        }
        
        else{
            if(args.action === 'add'){
                args.instance.addFinishedTopics({
                    elements: args.topics
                })
            }
            else{
                args.instance.deleteFinishedTopics({
                    elements: args.topics
                })
            }
        }

        args.instance.events.emit('update', args.instance.topics)
        return response
    })
}