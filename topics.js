const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    const self = this
    self.all = args.topics
    self.finished = {}
    self.pending = function(){
        return _.omit(self.all, _.keys(self.finished))
    }
}