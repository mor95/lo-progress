const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.instance))
        throw new Error('Invalid loProgress instance')

    if(args.setProgressBarAmount !== false)
        args.instance.setProgressBarAmount()


    if(args.updateVisualClue !== false)
        args.instance.updateVisualClue()

}