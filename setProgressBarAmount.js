const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')
    
    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')
    
    if(!_.isNumber(args.amount)){
        var filtered =  _.pick(args.instance.topics.all, _.keys(args.instance.topics.finished))
        args.amount = _.size(filtered) * 100 / _.size(args.instance.topics.all)
    }
    else{
        if(args.amount < 0 || args.amount > 100)
            throw new Error('Invalid amount to set progress bar to')
    }

    args.instance.updateVisualClue()

    _.forEach(args.instance.elements, function(element, key){
        const innerBars = element.querySelectorAll('.inner-bar')
        if(innerBars instanceof NodeList){
            _.forEach(innerBars, function(_element){
                _element.style.width = args.amount + '%'
            })
        }
    })

    return args.instance.elements
}