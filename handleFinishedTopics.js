const _ = require('lodash')

module.exports = function(args){
    if(!_.isObject(args))
        throw new Error('Invalid arguments')

    if(!_.isObject(args.elements))
        throw new Error('Invalid elements')

    if(!_.isObject(args.instance))
        throw new Error('Invalid instance')

    if(!_.isObject(args.instance.topics.finished))
        args.instance.topics.finished = {}
}